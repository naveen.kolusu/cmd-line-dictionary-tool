var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();
var async = require('async');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var oddWordsArray = ['aa', 'bibliopole', 'canorous', 'colporteur', 'commensalism', 'criticaster', 'digerati', 'e-tailer', 'karateka',
                      'lobola', 'misogamy', 'nacarat', 'nagware', 'netizen', 'noctambulist', 'omphalos', 'orrery', 'paludal', 'panurgic', 'plew',
                    'pollex', 'ragtop', 'retiform', 'toplofty', 'scofflaw', 'dragoman', 'effable', 'douceur', 'comminatory','concinnity', 'deedy',
                    'cerulean', 'chad', 'cryptozoology', 'etui', 'extramundane', 'famulus', 'petcock', 'winebibber', 'woopie'];

var Dictionary = require("oxford-dictionary-api");
var app_id = "cff4d233";
var app_key = "86aa247ed3766b3fed82644b6674ae70";
var requiredResult = process.argv[2];
var word = process.argv[3];

function reqFunction(urlFormed, callback){

  request(
      {
          method: 'GET',
          url: urlFormed,
          headers: {
              "content-type": "application/json",
              "app_id": app_id ,
              "app_key": app_key
          },
          json: true,
      }, function (error, response, body) {
          if (error) {
              return console.log("error:", error);
          }
          var str = body;
          if(body.results){
            callback(body.results[0]);
          }else{
            console.log("\n***This word is Not Found in Our Dictionary***\n");
          }
      });
}
function getSynonyms(word){
  if(!word){
    console.log('*****Please Enter Valid Word*****\n');
  }else{
    var urlFormed = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/en/' + word +'/synonyms'
    reqFunction(urlFormed, function(result){
      var synArray = result.lexicalEntries[0].entries[0].senses[0].synonyms
      var result = [];
      for(var i =0; i< synArray.length; i++){
        result.push(synArray[i].text);
      }
      if(result.length <=0){
        var result = "Synonyms are not availabe...\n"
      }else{
        console.log("The Synonyms are...\n", result);
      }

    })
  }
}
function getAntonyms(word){
  if(!word){
    console.log('*****Please Enter Valid Word*****\n');
  }else{
    var urlFormed = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/en/' + word +'/antonyms'
    reqFunction(urlFormed, function(result){
      var arr = result.lexicalEntries[0].entries[0].senses;
      var result = [];
      for(var j=0; j< arr.length; j++){
        var antArray = arr[j].antonyms
        for(var i =0; i< antArray.length; i++){
          result.push(antArray[i].text);
        }
      }
      if(result.length <=0){
        var result = "Antonyms are not availabe...\n"
      }else{
        console.log("The Antonyms are...\n", result);
      }

    })
  }
}
function getExample(word){
  if(!word){
    console.log('*****Please Enter Valid Word*****\n');
  }else{
    var urlFormed = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/en/' + word +'/sentences'
    reqFunction(urlFormed, function(result){
      var example = result.lexicalEntries[0].sentences[0].text;
      console.log("The Example is...\n", example);
    })
  }

}
function getDefinition(wordForDef){
  if(!wordForDef){
    console.log('*****Please Enter Valid Word*****\n');
  }else{
      var urlFormed = 'https://od-api.oxforddictionaries.com/api/v1/entries/en/' + wordForDef ;
    reqFunction(urlFormed, function(result){
        console.log("The definition of ",wordForDef, "is... \n", result.lexicalEntries[0].entries[0].senses[0].definitions[0]);
    })
  }

}
if(requiredResult == "def"){
  getDefinition(word);
}
else if(requiredResult == "ex"){
  getExample(word);
}
else if(requiredResult == "syn"){
  getSynonyms(word)
}
else if(requiredResult == "ant"){
  getAntonyms(word);
}
else if(word && requiredResult == 'dict'){
  getDefinition(word);
  getExample(word);
  getSynonyms(word);
  getAntonyms(word);

}
else if(!word && requiredResult && requiredResult != "play" && requiredResult != "syn" && requiredResult != "ant" && requiredResult != "ex" && requiredResult != "def" ){
  var word = requiredResult;
  getDefinition(word);
  getExample(word);
  getSynonyms(word);
  getAntonyms(word);
}
else if(!word && !requiredResult){
  var rand = oddWordsArray[Math.floor(Math.random() * oddWordsArray.length)];
  console.log("The word of the day is...", rand);
  var url = 'https://od-api.oxforddictionaries.com/api/v1/entries/en/' + rand ;
  reqFunction(url, function(result){
      console.log("The definition of ", rand , "is... \n ", result.lexicalEntries[0].entries[0].senses[0].definitions[0]);
  })
}
else if(requiredResult == "play" && !word){
  var standard_input = process.stdin;
  standard_input.setEncoding('utf-8');
  var randomWord;
  function getRandomDef(){
    randomWord = oddWordsArray[Math.floor(Math.random() * oddWordsArray.length)];
    var url = 'https://od-api.oxforddictionaries.com/api/v1/entries/en/' + randomWord
    reqFunction(url, function(result){
        console.log("Definition is: \n", result.lexicalEntries[0].entries[0].senses[0].definitions[0]);
    })
  }

  standard_input.on('data', function (data) {
      if(data === 'quit\n'){
          console.log("\n The Correct Word is :", randomWord);
          getExample(randomWord);
          getSynonyms(randomWord);
          getAntonyms(randomWord);
          setTimeout(function(){
          console.log("\n The game is exiting.. Bye Bye");
            process.exit();
          }, 1000);


      }else if(data === randomWord+'\n'){
        console.log("Congratulations!!!... You Have Done a great Job")
        getRandomDef();
      }
      else{
        var temp = randomWord;
        var letter = temp;
        var jumbledWord = "";
        for (var i = 0; i < temp.length; i++) {
            var index = Math.floor(Math.random() * letter.length);
            jumbledWord = jumbledWord + letter.charAt(index);
            letter = letter.substr(0, index) + letter.substr(index + 1);
        }
        console.log("Try Again with this hint : ", jumbledWord)
      }
  });
getRandomDef();
}
else{
  console.log("\n*****Not a Valid Input *****\n");
}
